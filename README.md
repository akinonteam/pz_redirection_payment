# pz_redirection_payment
A common library for enabling any payment option with redirection type's at order/payment step.  
## Usage
### 1. Install the app
At project root, create a requirements.txt if it doesn't exist and add the following line to it;
```bash
# For specific version (Recommended)
-e git+https://git@bitbucket.org/akinonteam/pz_redirection_payment.git@a412ac9#egg=pz_redirection_payment
```
For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.
```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package
```bash
# in /templates

# For specific version (Recommended)
yarn add ​git+ssh://git@bitbucket.org:akinonteam/pz_redirection_payment.git#a412ac9
```
Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project
```python
# omnife_base/settings.py:
INSTALLED_APPS.append('pz_redirection_payment')
```

### 4. Import template:
```jinja
{# in templates/orders/checkout/payment/index.html #}
{% from 'pz_redirection_payment/index.html' import PayWithRedirectionOption %}

{# Example using scenario at tabs structure #}

{# Should use where there are tab buttons #}
{# Use data-pk (same as matched tab content element's) attribute for redirection payment options. #}
<button class="checkout-payment-type__button js-payment-tab" data-type="redirection" data-pk="000" hidden>
  {{ _('Xyz Payment') }}
</button>

{# Should use where there are tab contents #}
{# Use data-pk (same as matched tab button element's) attribute for redirection payment options. #}
<div class="js-payment-tab-content" data-type="redirection" data-pk="000" hidden>
  {# Set paymentPk parameter same as tab button's and tab content's data-pk attribute. It's required. #}
  {{ PayWithRedirectionOption(paymentPk=000) }}
</div>

{# data-pk attribute, required for both of tab button and tab content when using redirection payments. #}
{# paymentPk parameter, required for using PayWithRedirectionOption macro. #}
{# Otherwise the redirection payment method will not work. #}
```

### 5. Import and initialize JS
```js
// in templates/orders/checkout/payment/index.js
import PayWithRedirectionOption from 'pz_redirection_payment';

class PaymentTab {
  activePaymentOptions = [
    // Other payment options...
    {
      type: 'redirection',
      pk: 000, // Related payment option pk. As same as own tab button, own tab content and own macro.
      handler: PayWithRedirectionOption // Imported class. Stay same for all redirection payments.
    },
    {
      type: 'redirection',
      pk: 111, // Related payment option pk. As same as own tab button, own tab content and own macro.
      handler: PayWithRedirectionOption // Imported class. Stay same for all redirection payments.
    }
  ];
}
// Each pk value, must be same with own tab button, own tab contents and own macro.
```

### 6. Import styles:
```scss
// in templates/orders/checkout/payment/index.scss
@import '~pz_redirection_payment/';
```

## Customizing the Html (Jinja Macro)
```jinja
{# Simple Using #}
{{ PayWithRedirectionOption(paymentPk=000) }}

{# Passing Parameters (w/ Default Parameters) #}
{{ PayWithRedirectionOption(
  paymentPk=000,
  paymentName='Xyz Payment',
  formClass='checkout-pay-with-redirection js-pay-with-redirection-form',
  title=_('Pay With {0}').format(paymentName),
  descriptions=[
    _('You can quickly and easily pay and complete your order with {0}.').format(paymentName),
  ],
  submitText=_('Pay With {0}').format(paymentName),
  agreementText=_('Check here to indicate that you have read and agree to the all terms.'),
  service='Checkout',
  action='completeRedirectionPayment'
) }}
```

All of the following are required or optional parameters for the Jinja2 macro.  
- **paymentPk**: (String / Integer, *Required*)  
- **paymentName**: (String / Gettext, *Optional*)  
- **formClass**: (String, *Optional*)  
- **title**: (String / Gettext, *Optional*)  
- **descriptions**: (Array, *Optional*)  
**|--> each description**: (String / Gettext)  
- **submitText**: (String / Gettext, *Optional*)  
- **agreementText**: (String / Gettext, *Optional*)  
- **service**: (String, *Optional*)  
- **action**: (String, *Optional*)  

## JS settings
Pass an object, which will be taken as destructured parameters, to customize things on the JS class.  

```js
// Simple Using
{
  type: 'redirection',
  pk: 000,
  handler: PayWithRedirectionOption
},

// Passing Parameters (w/ Default Parameters)
{
  type: 'redirection',
  pk: 000,
  handler: PayWithRedirectionOption,
  settings: {
    formSelector: '.js-pay-with-redirection-form',
    formErrorSelector: '.js-pay-with-redirection-form-errors',
    agreementInputSelector: '.js-pay-with-redirection-agreement-input',
    agreementErrorSelector: '.js-pay-with-redirection-agreement-error',
    completeButtonSelector: '.js-pay-with-redirection-complete-button',
    noRedirectUrlError: gettext('An error occured. Please try again later or contact site administrator.'),
  }
},
```
- **formSelector**: (String, *Optional*)  
- **formErrorSelector**: (String, *Optional*)  
- **agreementInputSelector**: (String, *Optional*)  
- **agreementErrorSelector**: (String, *Optional*)  
- **completeButtonSelector**: (String / Gettext, *Optional*)  