import { observe, store } from "shop-packages";
import {
  clearErrors,
  setAgreement,
  setErrors,
} from "shop-packages/redux/checkout/reducer";
import { setPaymentOption } from "shop-packages/redux/checkout/actions";
import {
  selectAgreement,
  selectErrors,
  selectPending,
  selectPreOrder,
} from "shop-packages/redux/checkout/selectors";

import { showRedirectionIframe } from "./redirection-iframe";

const trans = {
  noRedirectUrlError: gettext(
    "An error occured. Please try again later or contact site administrator."
  ),
};

class PayWithRedirectionOption {
  observers = [];

  constructor(option, settings = {}) {
    const settingValues = {
      formSelector: ".js-pay-with-redirection-form",
      formErrorSelector: ".js-pay-with-redirection-form-errors",
      agreementInputSelector: ".js-pay-with-redirection-agreement-input",
      agreementErrorSelector: ".js-pay-with-redirection-agreement-error",
      completeButtonSelector: ".js-pay-with-redirection-complete-button",
      noRedirectUrlError: trans.noRedirectUrlError,
      ...settings,
    };

    this.form = document.querySelector(
      `${settingValues.formSelector}[data-pk="${option.pk}"]`
    );

    if (this.form) {
      this.formError = this.form.querySelector(settingValues.formErrorSelector);
      this.agreementInput = this.form.querySelector(
        settingValues.agreementInputSelector
      );
      this.agreementError = this.form.querySelector(
        settingValues.agreementErrorSelector
      );
      this.completeButton = this.form.querySelector(
        settingValues.completeButtonSelector
      );
      this.noRedirectUrlError = settingValues.noRedirectUrlError;

      this.initObservers();
      this.bindEvents();
    }
  }

  // Action Events
  onAgreementChange = (e) =>
    store.dispatch(setAgreement(e.currentTarget.checked));

  onValidationChange = (response) =>
    (this.agreementError.hidden = response.detail.valid);

  onActionSuccess = (response) => {
    const { data } = response.detail.response;
    const { context_list, errors } = data;

    if (errors) {
      setErrors(errors);
    }

    const context = context_list?.find(
      (context) => context.page_slug === "redirectionpagecompletepage"
    );
    const redirectUrl = context?.page_context.redirect_url;
    const isIframe = context?.page_context.is_iframe || false;

    if (!redirectUrl) {
      this.setErrors(this.noRedirectUrlError);
      return;
    }

    if (isIframe) {
      showRedirectionIframe(redirectUrl);
    } else {
      window.location = redirectUrl;
    }
  };

  onActionError = (error) => setErrors(error);

  // Observer Events
  onAgreementUpdate = (isChecked) => (this.agreementInput.checked = isChecked);

  onPendingUpdate = (isPending) => (this.completeButton.busy = isPending);

  onErrorsUpdate = (errors) => {
    const errorsHolder = this.formError;

    errorsHolder.hidden = errors == null || errors?.length == 0;
    errorsHolder.innerHTML = "";

    for (const key in errors) {
      const error = errors[key];

      errorsHolder.append(Array.isArray(error) ? error[0] : error);
    }
  };

  onPreOrderUpdate = (isPreOrder) => {
    if (isPreOrder?.is_redirected) {
      const paymentPk = isPreOrder.payment_option?.pk;

      if (paymentPk) {
        store.dispatch(setPaymentOption(paymentPk));
      }
    }
  };

  // Helpers
  setErrors = (errors) => {
    store.dispatch(clearErrors());
    store.dispatch(setErrors(errors));
  };

  initFormValidations = () => {
    const requiredValidation = this.agreementInput.validations.find(
      (v) => v.type === "required"
    );

    if (requiredValidation) {
      requiredValidation.errorPlacement = this.agreementError;
    }
  };

  // Main
  bindEvents = () => {
    this.initFormValidations();
    this.agreementInput.addEventListener("input", this.onAgreementChange);
    this.agreementInput.addEventListener(
      "validation-change",
      this.onValidationChange
    );
    this.form.addEventListener("action-success", this.onActionSuccess);
    this.form.addEventListener("action-error", this.onActionError);
  };

  initObservers = () => {
    this.observers = [
      observe(selectAgreement).subscribe(this.onAgreementUpdate),
      observe(selectPending).subscribe(this.onPendingUpdate),
      observe(selectErrors).subscribe(this.onErrorsUpdate),
      observe(selectPreOrder).subscribe(this.onPreOrderUpdate),
    ];
  };

  unmount = () => {
    if (this.form) {
      this.agreementInput.removeEventListener("input", this.onAgreementChange);
      this.agreementInput.removeEventListener(
        "validation-change",
        this.onValidationChange
      );
      this.form.removeEventListener("action-success", this.onActionSuccess);
      this.form.removeEventListener("action-error", this.onActionError);
      this.observers?.forEach((observer) => observer.unsubscribe());
      this.agreementInput.checked = false;
      [this.agreementError, this.formError].forEach((element) => {
        element.innerHTML = "";
        element.hidden = true;
      });
    }

    store.dispatch(setAgreement(false));
    store.dispatch(clearErrors());
  };
}

export default PayWithRedirectionOption;
