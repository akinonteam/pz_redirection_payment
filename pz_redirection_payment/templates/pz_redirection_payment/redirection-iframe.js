const selectWrapper = () =>
  document.querySelector(
    '.js-payment-tab-content[data-type="redirection"]:not([hidden])'
  );

const createIframeElements = (redirectUrl, removeIframe) => {
  const iframeWrapper = document.createElement("div");
  const iframe = document.createElement("iframe");
  const closeButton = document.createElement("div");

  iframeWrapper.className =
    "checkout-payment-redirection-iframe-wrapper js-checkout-payment-redirection-iframe-wrapper";
  closeButton.className =
    "checkout-payment-redirection-iframe-wrapper-close-button js-checkout-payment-redirection-iframe-close-button";

  iframe.setAttribute("src", redirectUrl);
  closeButton.innerHTML = "&#x2715";
  closeButton.addEventListener("click", removeIframe);

  iframeWrapper.append(iframe, closeButton);

  return { iframeWrapper, iframe };
};

const iframeURLChange = (iframe, callback) => {
  iframe.addEventListener("load", () => {
    setTimeout(() => {
      if (iframe?.contentWindow?.location) {
        callback(iframe.contentWindow.location);
      }
    }, 0);
  });
};

const removeIframe = async (wrapper, form) => {
  const iframeSelector = document.querySelector(
    ".js-checkout-payment-redirection-iframe-wrapper"
  );

  if (!iframeSelector) {
    return;
  }

  iframeSelector.remove();

  const iframeLoading = document.createElement("div");
  iframeLoading.className = "iframe-loading-wrapper js-iframe-loading-wrapper";
  wrapper.appendChild(iframeLoading);

  await fetch(`${window.URLS.checkout}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
    },
  });

  iframeLoading.remove();

  if (form) {
    form.style.display = "block";
  }
};

const handleIframeRedirection = (iframe, removeIframe) => {
  iframeURLChange(iframe, (location) => {
    if (location.origin !== window.location.origin) {
      return false;
    }

    const searchParams = new URLSearchParams(location.search);
    const isOrderCompleted = location.href.includes("/orders/completed");
    const data = {
      url: location.pathname,
    };

    if (searchParams.has("success") || isOrderCompleted) {
      removeIframe();
    }

    if (isOrderCompleted) {
      window.parent.postMessage(JSON.stringify(data));
    }
  });
};

export const showRedirectionIframe = (redirectUrl) => {
  const wrapper = selectWrapper();
  const form = wrapper.querySelector("form");
  const { iframeWrapper, iframe } = createIframeElements(redirectUrl, () =>
    removeIframe(wrapper, form)
  );

  const iframeSelector = document.querySelector(
    ".js-checkout-payment-redirection-iframe-wrapper"
  );

  if (!iframeSelector) {
    if (form) {
      form.style.display = "none";
    }
    wrapper.appendChild(iframeWrapper);
    handleIframeRedirection(iframe, () => removeIframe(wrapper, form));
  }
};
